/**
 *
 * @author rushil
 */
public class BmiSimulation {
    public static void main(String[] args) {
        
        Person person1 = new Person("John Smith", 82, Weight.K, 1.72, Height.M);
        Person person2 = new Person("Maria Wang", 152, Weight.LB, 70, Height.IN);
        
        System.out.println("BMI for Person 1: " + person1.getBMI());
        System.out.println("BMI for Person 2: " + person2.getBMI());
    }
}
