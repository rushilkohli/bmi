/**
 *
 * @author rushil
 */
public class MVCSimulation {
    
        
    public static void main(String[] args) {
        Student model = retrieveStudentFromDatabase();
        StudentView view = new StudentView();
        StudentController controller = new StudentController(model, view);
        controller.updateView();
        controller.setStudentName("Conor Mcregor");
        controller.setStudentId("18");
        controller.updateView();
        
    }
    
    private static Student retrieveStudentFromDatabase() {
        Student student = new Student("Mike Tyson", "22");
        return student;
    }
}
