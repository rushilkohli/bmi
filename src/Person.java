/**
 *
 * @author rushil
 */
public class Person {
    private String name;   
    private double weight;
    private double height;
    private Weight unitWeight;
    private Height unitHeight;
    
    public Person(String name, double weight, Weight unitWeight, double height, Height unitHeight) {
        this.name = name;
        this.weight = weight;
        this.height = height;
        this.unitHeight = unitHeight;
        this.unitWeight = unitWeight;       
    }
    
    public Person(String name) {
        this.name = name;
    }    
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public double getHeight() {
        return height;
    }

    public double getBMI() { 
       double weightInKilos = 0;
       double weightInPounds = 0;
       double heightInMetres = 0;
       double heightInInches = 0;
       if(unitWeight == Weight.K && unitHeight == Height.M) {
            weightInKilos = weight;
            heightInMetres = height;
            return weightInKilos/ (Math.pow(heightInMetres, 2));
        }
       else if(unitWeight == Weight.LB && unitHeight == Height.IN) {
            weightInPounds = weight;
            heightInInches = height;
            return weightInPounds * 0.4535 / Math.pow(heightInInches* 0.0254 , 2);
        }
       else {
           return 0;
       }      
    }
}
