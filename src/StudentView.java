/**
 *
 * @author rushil
 */
public class StudentView {
    
    public void printStudentDetails(Student model) {
        System.out.println("Name: " + model.getName());
        System.out.println("Student Id: " + model.getId());
    }
}
